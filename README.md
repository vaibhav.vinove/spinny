# Spinny Django REST API

### Functionality Covered : 
 - Add
 - List ALL
 - List My
 - Update
 - Delete

# Filters:
 - Boxes with length_more_than or length_less_than
 - Boxes with breadth_more_than or breadth_less_than
 - Boxes with height_more_than or height_less_than
 - Boxes with area_more_than or area_less_than
 - Boxes with volume_more_than or volume_less_than

# Permissions:
 - Only Staff user shall be able to see his/her created boxes in the store
 - User should be logged in and should be staff to add the box

# URL's : 
```sh
    1. Add Api                      : 127.0.0.1:8000/task/add/     
    2. List all Api                 : 127.0.0.1:8000/task/listAll/
    3. List all Api(using filters)  : 127.0.0.1:8000/task/listAll/?area__gt=27
    4. List my boxes Api            : 127.0.0.1:8000/task/listMy/
    5. List my boxes(using filters) : 127.0.0.1:8000/task/listMy/?volume__gt=27
    6. Update Api                   : 127.0.0.1:8000/task/update/8/
    7. Delete Api                   : 127.0.0.1:8000/task/delete/9/
```

# HTTP staus response : 
    200, 400, 404
    
# Filters query :
 We are considering the URL : 127.0.0.1:8000/task/listAll/ to explain working of filters.
 
Boxes with :
 - length_more_than : 127.0.0.1:8000/task/listAll/?length__gt=27
 - length_less_than : 127.0.0.1:8000/task/listAll/?length__lt=27
 - breadth_more_than :127.0.0.1:8000/task/listAll/?width__gt=27
 - breadth_less_than :127.0.0.1:8000/task/listAll/?width__lt=27
 - height_more_than : 127.0.0.1:8000/task/listAll/?height__gt=27
 - height_less_than : 127.0.0.1:8000/task/listAll/?height__lt=27
 - area_more_than :   127.0.0.1:8000/task/listAll/?area__gt=27
 - area_less_than :   127.0.0.1:8000/task/listAll/?area__lt=27
 - volume_more_than : 127.0.0.1:8000/task/listAll/?volume__gt=27
 - volume_less_than : 127.0.0.1:8000/task/listAll/?volume__lt=27

### Note : 
Similar query string can be appended in List my boxes Api : 127.0.0.1:8000/task/listMy/

