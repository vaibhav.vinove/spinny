from django.db import models
from django.conf import settings
from django.db.models import Sum
from django.conf import settings
from django.db.models import Q
from datetime import datetime, timedelta

from django.contrib.auth.models import User
from django.core.exceptions import ValidationError 

# Create your models here.


class Box(models.Model):
    length = models.FloatField(null=False, blank=False)
    width = models.FloatField(null=False, blank=False)
    height = models.FloatField(null=False, blank=False)
    area = models.FloatField(null=True, blank=False)
    volume = models.FloatField(null=True, blank=False)
    #attribute editable=False to disallow change
    created_by = models.ForeignKey(User, null=True, blank=False, editable=False, on_delete=models.SET_NULL)
    #created_on will be autofilled with present date
    created_on = models.DateTimeField(auto_now_add=True, null=False,editable=False)
    # last_updated will be updated automatically as auto_now=true 
    last_updated = models.DateTimeField(auto_now=True)


    def validate(self,area,volume,created_by):
        averageArea = (((Box.objects.all().aggregate(Sum('area')))['area__sum']+float(area))/((Box.objects.all()).count())+1.0)
        if averageArea > settings.A1:
            raise ValidationError("Average area of all added boxes should not exceed "+ str(settings.A1))
        averageVolume = ((Box.objects.filter(created_by=created_by).aggregate(Sum('volume')))['volume__sum']+float(volume))/((Box.objects.filter(created_by=created_by).count())+1.0)
        if averageVolume > settings.V1 :
            print ('in valid error')
            raise ValidationError("Average volume of all boxes added by the current user shall not exceed "+ str(settings.V1))
        totalBoxCount = Box.objects.filter(Q(created_on__gte =(datetime.today() - timedelta(days=7)))).count()
        if totalBoxCount > settings.L1 :
            raise ValidationError("Total Boxes added in a week cannot be more than "+ str(settings.L1))
        userBoxCount = Box.objects.filter(Q(created_by=created_by) & Q(created_on__gte =(datetime.today() - timedelta(days=7)))).count()
        if userBoxCount > settings.L2 : 
            raise ValidationError("Total Boxes added in a week by a user cannot be more than "+ str(settings.L2))


    def save(self, *args, **kwargs):
        self.area = 2*((self.length * self.width) + 
                        (self.width * self.height) + 
                        (self.length * self.height))
        self.volume = (self.length * self.height * self.width)
        self.validate(self.area,self.volume,self.created_by)
        super(Box, self).save(*args, **kwargs)