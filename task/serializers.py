from rest_framework import serializers
from task.models import Box

class addBoxSerializer(serializers.ModelSerializer):
    class Meta:
        model = Box
        fields = [
            'id',
            'length',
            'width',
            'height',
            'created_by',
        ]

class ListSerializer(serializers.ModelSerializer):
    created_by = serializers.SerializerMethodField()
    created_on = serializers.SerializerMethodField()


    class Meta:
        model = Box
        fields = [
            'id',
            'length',
            'width',
            'height',
            'area',
            'volume',
            'created_by',
            'created_on'
        ]


    def get_created_by(self,obj):
        request = self.context['request']
        if request.user.is_staff:
            return str(obj.created_by)
        else :
            return None

    def get_created_on(self,obj):
        request = self.context['request']
        if request.user.is_staff:
            return str(obj.created_on)
        else :
            return None

class AllSerializer(serializers.ModelSerializer):
    class Meta:
        model = Box
        fields = '__all__'
