from django_filters.rest_framework import DjangoFilterBackend
from task.permissions import IsStaffPermission
from rest_framework import status as errstatus
from rest_framework.response import Response
from django.contrib.auth.models import User
from rest_framework.views import APIView
from rest_framework import generics
from django.shortcuts import render
from django.utils import timezone
from django.conf import settings
from django.db.models import Q
from task.models import Box
from task.serializers import *

# Create your views here.

class BoxAPI(generics.CreateAPIView):
    serializer_class = addBoxSerializer
    permission_classes = [IsStaffPermission]
    def create(self, request):
        try :
            reqData = request.data.copy()
            serializer = self.get_serializer(data=reqData)
            if serializer.is_valid():serializer.save(created_by=request.user)
            return Response({"message": "Box created Successfuly!", "status": 200, "statusType": "Success",'data':serializer.data})
        except Exception as e:
            return Response({"message": e.args[0], "status": errstatus.HTTP_400_BAD_REQUEST, "statusType": "Failed"},errstatus.HTTP_400_BAD_REQUEST)


class UpdateBox(generics.UpdateAPIView):
    serializer_class = ListSerializer
    queryset = Box.objects.all()
    permission_classes = [IsStaffPermission]
    def perform_update(self, serializer):
        try:
            reqData = self.request.data.copy()
            if serializer.is_valid():serializer.save()
        except Exception as e:
            return Response({"message": e.args[0], "status": errstatus.HTTP_400_BAD_REQUEST, "statusType": "Failed"},errstatus.HTTP_400_BAD_REQUEST)


class ListAll(generics.ListAPIView):
    serializer_class = ListSerializer
    queryset = Box.objects.all()
    filters = ['gte', 'lte', 'exact', 'gt', 'lt']
    filter_backends = [DjangoFilterBackend]
    filterset_fields = {'length' : filters, 'width' : filters , 'height' : filters , 'area' : filters, 'volume' : filters}


class ListMy(generics.ListAPIView):
    serializer_class = ListSerializer
    permission_classes = [IsStaffPermission]
    filters = ['gte', 'lte', 'exact', 'gt', 'lt']
    filter_backends = [DjangoFilterBackend]
    filterset_fields = {'length' : filters, 'width' : filters , 'height' : filters , 'area' : filters, 'volume' : filters}

    def get_queryset(self, *args, **kwargs):
        return Box.objects.filter(Q(created_by=self.request.user))


class DelBox(generics.DestroyAPIView):
    queryset = Box.objects.all()
    def destroy(self, request, *args, **kwargs):
        try:
            instance = self.get_object()
            if instance.created_by != request.user:status, msg = 0, 'You are not a creator of this box'
            else : 
                instance.delete()
                status, msg = 1, "Deleted Successfuly"
            if status == 1 : return Response({"message": msg, "status": 200, "statusType": "success"})
            else : return Response({"message": msg, "status": 400, "statusType": "failed"},errstatus.HTTP_400_BAD_REQUEST)
        except Exception as e:
            return Response({"message": e.args[0], "status": errstatus.HTTP_400_BAD_REQUEST, "statusType": "Failed"},errstatus.HTTP_400_BAD_REQUEST)