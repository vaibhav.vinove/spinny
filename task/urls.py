from django.urls import path
from . import views

# apis for CRUD
urlpatterns = [
    path('/add/', views.BoxAPI.as_view(), name='box-create'),
    path('/update/<pk>/', views.UpdateBox.as_view(), name='update-box'),
    path('/listAll/', views.ListAll.as_view(), name='box-list'),
    path('/listMy/', views.ListMy.as_view(), name='box-list'),
    path('/delete/<pk>/', views.DelBox.as_view(), name='delete'),

]